import React, { Component } from 'react'
import BodyComponent from '../BodyComponent/BodyComponent'
import FooterComponent from '../FooterComponent/FooterComponent'
import HeaderComponent from '../HeaderComponent/HeaderComponent'

export default class BaiTapLayoutComponent extends Component {
  render() {
    return (
      <div>
        {/* <p>BaiTapLayoutComponent</p> */}
        <HeaderComponent />
        <BodyComponent />
        <FooterComponent />
      </div>
    )
  }
}
